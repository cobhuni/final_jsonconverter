#!/bin/bash
#
#     convert.sh
#
# Collect last annotated version of COBHUNI xmi files and convert them into json
#
# usage:
#   $ bash convert.sh
#
# for testing:
#   $ python getlastversions.py ../../data/files/original/ocred_texts ../../data/files/inprogress/ocred_texts | xargs -L 1 java -cp target/dependency/*:target/final_jsonconverter-0.0.1-SNAPSHOT.jar final_jsonconverter/Pipeline -o ../../data/files/prepared/ocred_texts -i
#
################################################################################

#
# constants
#

PARENT_DIR="../../data/files"

ORI_DIR="$PARENT_DIR/original"
ANNOT_DIR="$PARENT_DIR/inprogress"
OUT_DIR="$PARENT_DIR/prepared"

OCRED_DIR="ocred_texts"
ALTAFSIR_DIR="altafsir"
HADITH_DIR="hadith_alislam"

JAR="target/dependency/*:target/final_jsonconverter-0.0.1-SNAPSHOT.jar"

#
# main
#

#for MYDIR in $OCRED_DIR $HADITH_DIR $ALTAFSIR_DIR ; do 
#    
#    # for each subdir, get stream of file paths to convert
#    python getlastversions.py "$ORI_DIR/$MYDIR" "$ANNOT_DIR/$MYDIR" |
#    
#    # convert to json and save into output directory
#    xargs -L 1 java -cp $JAR final_jsonconverter/Pipeline -o "$OUT_DIR/$MYDIR" -i
#
#done

#
# process OCRED files
#

## for each subdir, get stream of file paths to convert
#python getlastversions.py "$ORI_DIR/$OCRED_DIR" "$ANNOT_DIR/$OCRED_DIR" --strict |
#    
## convert to json and save into output directory
#xargs -L 1 java -cp $JAR final_jsonconverter/Pipeline -o "$OUT_DIR/$OCRED_DIR" -i

#
# process ALTAFSIR files
#

# for each subdir, get stream of file paths to convert
python getlastversions.py "$ORI_DIR/$ALTAFSIR_DIR" "$ANNOT_DIR/$ALTAFSIR_DIR" --strict |
    
# convert to json and save into output directory
java -cp $JAR final_jsonconverter/Pipeline -i  -o "$OUT_DIR/$ALTAFSIR_DIR"

#
# process HADITH ALISLAM files
#

## for each subdir, get stream of file paths to convert
#python getlastversions.py "$ORI_DIR/$HADITH_DIR" "$ANNOT_DIR/$HADITH_DIR" --strict |
#    
## convert to json and save into output directory
#xargs -L 1 java -cp $JAR final_jsonconverter/Pipeline -o "$OUT_DIR/$HADITH_DIR" -i

