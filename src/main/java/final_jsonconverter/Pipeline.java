package final_jsonconverter;

import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.factory.CollectionReaderFactory.createReaderDescription;
import static org.apache.uima.fit.pipeline.SimplePipeline.runPipeline;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpSegmenter;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiReader;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiWriter;

import de.tudarmstadt.ukp.dkpro.core.io.xml.XmlReader;
import de.tudarmstadt.ukp.dkpro.core.io.xml.XmlWriterInline;

import de.tudarmstadt.ukp.dkpro.core.io.text.TextReader;
import de.tudarmstadt.ukp.dkpro.core.io.text.TextWriter;

import de.tudarmstadt.ukp.dkpro.core.io.tcf.TcfWriter;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;


public class Pipeline {
	
	@Parameter(names={"--inpath", "-i"})
	String inpath;
	
	@Parameter(names={"--outpath", "-o"})
    String outpath;

	public static void main(String[] args) throws Exception {
		
		 Pipeline pipeline = new Pipeline();
		 new JCommander(pipeline, args);
		 		
		 runPipeline(
		        createReaderDescription(XmiReader.class,
		                                XmiReader.PARAM_SOURCE_LOCATION, pipeline.inpath,
		                                XmiReader.PARAM_LANGUAGE, "en",
		                                XmiReader.PARAM_LENIENT, true),
		        
		        createEngineDescription(JsonWriter.class,
		        		                JsonWriter.PARAM_TARGET_LOCATION, pipeline.outpath));
	}

}
