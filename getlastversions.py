#!/usr/bin/env python
#
#     getlastversions.py
#
# Copy last version of cobhuni annotated files in temp folder for converting them to json in a following stage.
# 
# usage:
#   $ for DIR in {ocred_texts, hadith_alislam, ocred_texts} ; do 
#   $   python getlastversions.py ../../data/files/original/$DIR ../../data/files/inprogress/$DIR
#   $ done
#
##################################################################################################

import os
import sys
import argparse
import itertools
import operator
import shutil

#
# constants
#

MYPATH = os.path.dirname(os.path.realpath(__file__))

PARENT_TMPDIR   = os.path.join(MYPATH, 'tmp')
OCRED_TMPDIR    = os.path.join(PARENT_TMPDIR, 'ocred_texts')
ALTAFSIR_TMPDIR = os.path.join(PARENT_TMPDIR, 'altafsir')
HADITH_TMPDIR   = os.path.join(PARENT_TMPDIR, 'hadith_alislam')

#
# functions
#

def getfilelist(filesdir):
    """ Yield list of filenames from filesdir

    Args:
        files (str): Directory containing files.

    Yield:
        str, str: filepath and filename of xmi files.

    """
    # get path, name from objs within filesfir that are files
    fobjs = ((obj.path, *os.path.splitext(obj.name)) for obj in os.scandir(filesdir) if obj.is_file())

    # yield each file that is in xmi format
    yield from ((fpath, fname) for fpath, fname, fext in fobjs if fext=='.xmi')   

#
# main
#

# create temp folders
try:
    os.mkdir(PARENT_TMPDIR)
    os.mkdir(OCRED_TMPDIR)
    os.mkdir(ALTAFSIR_TMPDIR)
    os.mkdir(HADITH_TMPDIR)
except FileExistsError:
    pass


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Get last versions of annotated files')
    parser.add_argument('oridir',  metavar='ORI_DIR', help='parent directory for searching for not annotated files')
    parser.add_argument('anndir', metavar='ANNOT_DIR', help='parent directory for getting last version of annotated files')
    parser.add_argument('--strict', action='store_true', help='Get only files from annotation directory')
    args = parser.parse_args()

    # get original files and add a 0 indicating the version of the file
    if not args.strict:
        orifiles = ((fpath, fname, 0) for fpath, fname in getfilelist(args.oridir))
    else:
        orifiles = []

    try:
        # get all annotated files and separate the filename from the annotation info
        annfiles_info = ((fp, *fn.rsplit('_', 4)) for fp, fn in getfilelist(args.anndir))

        # keep path, name and version for each file
        annfiles = ((fp, fn, int(rest[-1])) for fp, fn, *rest in annfiles_info)

        # merge original and annotated files and sort them by file name
        allfiles = sorted(itertools.chain(orifiles, annfiles), key=operator.itemgetter(1))

    except ValueError:
        print('Fatal error: One or more files in annotation folder do not follow the expected name format.', file=sys.stderr)
        sys.exit(1)

    # group file info entry by name of file
    groups = itertools.groupby(allfiles, key=operator.itemgetter(1))

    # get last version of each group of files
    lastversion = (max(group, key=operator.itemgetter(2)) for key, group in groups)

    for fpath, fname, version in lastversion:

        print(fpath, fname, version)

        shutil.copyfile(fpath, fname+'.xmi')
