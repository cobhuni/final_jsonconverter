
PARENT_DIR=../../data/files/prepared

OUTFILES_OCRED=$(PARENT_DIR)/ocred_texts/*.json
OUTFILES_ALTAFSIR=$(PARENT_DIR)/altafsir/*.json
OUTFILES_HADITH=$(PARENT_DIR)/hadith_alislam/*.json



RM=/bin/rm -f

.PHONY : all clean help convert

all: clean convert

help:
	@echo "    all"
	@echo "        Clean resources and convert xmi files into json"
	@echo "    convert"
	@echo "        Convert xmi files into json"
	@echo "    clean"
	@echo "        Clean resources"
	@echo ""
	@echo "usage: make [help] [all] [clean]"

clean:
	mvn clean dependency:copy-dependencies package
	$(RM) $(OUTFILES_OCRED)
	$(RM) $(OUTFILES_ALTAFSIR)
	$(RM) $(OUTFILES_HADITH)

convert:
	bash convert.sh